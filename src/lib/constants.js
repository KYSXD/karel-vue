export const RunningState = {
  CLEAN: 0,
  RUNNING: 1,
  PAUSED: 2,
  FINISHED: 3,
};
