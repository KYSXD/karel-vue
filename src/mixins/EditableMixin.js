import { RunningState } from '../lib/constants';

export default {
  computed: {
    editable() {
      return this.state == RunningState.CLEAN;
    },
  },
};
