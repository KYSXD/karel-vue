# karel-vue

La _completamente nueva_ interfaz web para karel.

## Cómo desarrollar

```
npm install
npm run jison # compila archivos de gramática en parsers necesarios
npm run serve # inicia un servidor local en localhost:8080
```

## Cómo compilar para producción

```
npm run lint # para asegurar que el código sigue algunos principios básicos
npm run build
```
