# Cosas que planeo hacer:

* componente con slide para controlar la velocidad
* botón de reset se hace visible a sí mismo cuando el estado lo amerita
* mundo demuestra de alguna manera que es editable
* editor de texto se vuelve readonly cuando el programa está corriendo
* componente de la mochila es editable cuando el mundo es editable
* controles de reproducción se marcan como deshabilitados en ciertos estados
